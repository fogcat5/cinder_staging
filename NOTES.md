------------------------------

currentDataSet = resultObject['result']['clusterCapacity'];

['thinProvisioningFactor'] = Math.round(100 * (['nonZeroBlocks'] + ['zeroBlocks']) / ['nonZeroBlocks']) / 100;

thinProvisioningFactor = 100*(nonZeroBlocks + zeroBlocks)/nonZeroBlocks

deDuplicationFactor = Math.round(100 * ['nonZeroBlocks'] / ['uniqueBlocks']) / 100;
compressionFactor = Math.round(100 * ['uniqueBlocks'] * 4096 / Math.max(1, ['uniqueBlocksUsedSpace'])) / 100;
efficiencyFactor = Math.round(100 * (['thinProvisioningFactor'] * ['deDuplicationFactor'] * ['compressionFactor'])) / 100;


------------------------------




Well… answering my own question now.

No, there's not an API call for that.  It turns out that it's computed by the javascript on the web page.  The code is in 
https://10.57.160.32/admin/js/reporting/reportingSummary.js

Here's that function:
	
	function parseClusterCapacity(resultObject){
		if(resultObject['result'] == '{}' || !('clusterCapacity' in resultObject['result'])){
			currentDataSet = false;
			currentDataType = false;
			displayNotice('Cluster capacity stats are currently unavailable.');
			return;
		}
		
		currentDataSet = resultObject['result']['clusterCapacity'];
			
		currentDataSet['thinProvisioningFactor'] = Math.round(100 * (currentDataSet['nonZeroBlocks'] + currentDataSet['zeroBlocks']) / currentDataSet['nonZeroBlocks']) / 100;
		currentDataSet['deDuplicationFactor'] = Math.round(100 * currentDataSet['nonZeroBlocks'] / currentDataSet['uniqueBlocks']) / 100;
		currentDataSet['compressionFactor'] = Math.round(100 * currentDataSet['uniqueBlocks'] * 4096 / Math.max(1, currentDataSet['uniqueBlocksUsedSpace'])) / 100;
		currentDataSet['efficiencyFactor'] = Math.round(100 * (currentDataSet['thinProvisioningFactor'] * currentDataSet['deDuplicationFactor'] * currentDataSet['compressionFactor'])) / 100;
		
		if(!isFinite(currentDataSet['thinProvisioningFactor']) || isNaN(currentDataSet['thinProvisioningFactor']))
		currentDataSet['thinProvisioningFactor'] = '-';
		else
		currentDataSet['thinProvisioningFactor'] += 'x';
		
		if(!isFinite(currentDataSet['deDuplicationFactor']) || isNaN(currentDataSet['deDuplicationFactor']))
		currentDataSet['deDuplicationFactor'] = '-';
		else
		currentDataSet['deDuplicationFactor'] += 'x';
		
		if(!isFinite(currentDataSet['compressionFactor']) || isNaN(currentDataSet['compressionFactor']))
		currentDataSet['compressionFactor'] = '-';
		else
		currentDataSet['compressionFactor'] += 'x';
		
		if(!isFinite(currentDataSet['efficiencyFactor']) || isNaN(currentDataSet['efficiencyFactor']))
		currentDataSet['efficiencyFactor'] = '-';
		else
		currentDataSet['efficiencyFactor'] += 'x';
		
		$('#overallEfficiencyFactor').html(currentDataSet['efficiencyFactor']);
		$('#thinProvisioningFactor').html(currentDataSet['thinProvisioningFactor']);
		$('#deDuplicationFactor').html(currentDataSet['deDuplicationFactor']);
		$('#compressionFactor').html(currentDataSet['compressionFactor']);
		$('#peakActiveSessions').html(currentDataSet['peakActiveSessions']);
		$('#activeSessions').html(currentDataSet['activeSessions']);
		
		if(currentDataType != 'clusterSummary'){
			currentDataType = 'clusterSummary';
			
			IOChart = false;
			bandwidthChart = false;
			utilizationChart = false;
			delete aggregateStats['clusterStats'];
			callAPI('GetClusterStats');
		}
		else{
			if(provisionedChart && ($('#provisionedSpace').attr('provisionedSpace') != currentDataSet['provisionedSpace'] ||	$('#provisionedSpace').attr('unprovisionedSpace') != (currentDataSet['maxProvisionedSpace'] - currentDataSet['provisionedSpace'])))
			updateProvisionedChart(provisionedChart, currentDataSet);
			if(updateUsedChart && ($('#usedSpace').attr('usedSpace') != currentDataSet['usedSpace'] || $('#usedSpace').attr('unusedSpace') != (currentDataSet['maxUsedSpace'] - currentDataSet['usedSpace'])))
			updateUsedChart(usedChart, currentDataSet);
		}
		
		summaryPieChartTimeout = setTimeout(function(){ if(currentDataType == 'clusterSummary') callAPI('GetClusterCapacity'); }, 15000);
	}
	



I can work with that and figure out the same calculations in ruby to log the efficiency.
Thanks,
--Rodney


From: <Peck>, Rodney Peck <ropeck@paypal.com>
Date: Thursday, July 18, 2013 8:57 AM
To: Pratik Chavda <pratik.chavda@solidfire.com>, John Griffith <john.griffith@solidfire.com>
Subject: Soldfire rest api

John, Pratik -- 
  I'm working on the scripting of the overall cloning tests and I'd like to use the REST api on the Solidfire to collect the stats like efficiency, dedup and usage between each run.  I have it working for the usage and I can see most of the stats like blocks provisioned, zero blocks, etc.  But I can't find a api method to give me the items in the efficiency part of the status page. 

This part of the web page…


  I've used the GetClusterCapacity and GetClusterStats methods.  Is there one I can try for the other stats?

Thanks,
--Rodney
