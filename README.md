cinder_staging
==============

scripts used for cinder volume clone testing on the staging storage POCs


here's some random notes


INSTALLATION

How to install cinder_staging test scripts.

required packages
  ruby
  gem
  rest_client gem
  json gem
  openstack gem


$ sudo gem install rest-client
$ sudo gem install json
  note: on osx, without xcode, you can use json-pure which does json without compiling an extension
  it may be a little slower, but it's compatible with everything.  great for testing.
$ sudo gem install openstack
  note: on osx, native compiled gems require Xcode to be installed.  you can get it from the app store for free, but it is 1.5GB or more in size.



