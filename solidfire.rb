#!/usr/bin/ruby

require 'rubygems'
require 'rest-client'
require 'json'


class Solidfire
  Cols = "efficiencyFactor thinProvisioningFactor deDuplicationFactor compressionFactor provisionedSpace usedSpace".split(' ')
  def initialize(host_i, user_i, pass_i)
    host = host_i
    user = user_i
    pass = pass_i
    @url = "https://#{user}:#{pass}@#{host}/json-rpc/5.0/"
  end

  def post(cmd, params = nil)    
    request = Hash.new
    request['method'] = cmd
    request['id'] = Time.now.to_i
    request['params'] = params if params != nil
    r = RestClient.post @url, JSON.generate(request)

    JSON.parse(r)["result"]
  end
  def getClusterCapacity
    post("GetClusterCapacity")["clusterCapacity"]
  end

  attr_reader :thinProvisioningFactor, :compressionFactor, :deDuplicationFactor, :efficiencyFactor, :zeroBlocks, :provisionedSpace, :usedSpace

  def update

    def rounded(f, n=2)
      (f * (10**n)).round/10**n.to_f
    end

    st = getClusterCapacity
    @zeroBlocks = st["zeroBlocks"]
    @nonZeroBlocks = st["nonZeroBlocks"]
    @uniqueBlocks = st["uniqueBlocks"]
    @uniqueBlocksUsedSpace = st["uniqueBlocksUsedSpace"]
    @thinProvisioningFactor =rounded((@zeroBlocks + @nonZeroBlocks).to_f / @nonZeroBlocks)
    @deDuplicationFactor = rounded(@nonZeroBlocks.to_f / @uniqueBlocks)
    @compressionFactor = rounded(@uniqueBlocks * 4096.0 / @uniqueBlocksUsedSpace)
    @efficiencyFactor = rounded(@thinProvisioningFactor * @deDuplicationFactor * @compressionFactor)
    @provisionedSpace = rounded(st["uniqueBlocksUsedSpace"].to_f / 1024 /1024 )
    @usedSpace = rounded(@uniqueBlocksUsedSpace  /1024 /1024)
    @st = st
  end
  def csv
    update
    a = [Time.now.to_i]
    Cols.each do |x|
      a << eval(x)
    end
    a.join(", ")
  end
  def heading
    "time, " + Cols.join(", ")
  end
  def volumes
    post("ListActiveVolumes")["volumes"]
  end
end
