#!/usr/bin/ruby
require 'pp'
require 'aurora'

solid = Aurora_solidfire
vs = Volume_service

# first remove any existing test volumes
# vs.volumes.each do |v|
#   next unless /^test_clone/.match(v.display_name)
#   vs.delete_volume(v.id)
#   puts "deleted " + v.display_name
# end
# 
# create a volume or find it by name

# time how long it takes to clone that volume

# work out how many volumes exist.
# reset or clear the volumes out
# create 100 and time each/overall time

# check the solidfire status for efficiency, usage, etc. between runs
# save the results overall
# log_solidfire('clone volume') do 
#   vol.create(volname, source volume, size)
# end
#
# def log_solidfire(tag) 
#   st = Time.now
#   yield
#   t = Time.now - st
#   fetch_solidfire_data
#   write_log(tag, t, solidfirestats)
#

puts solid.heading
puts solid.csv
100.times do |n|
  name = sprintf("test_clone%03d",n)
  clone = vs.create_volume(:display_name => name,
	:volume_type => "solidfire",
	:size => 400,
	:source_volid => "4fdc1df1-f8b6-472b-ace7-bae3cbf23981")


  time_start = Time.now
  #puts "clone status #{clone.status}" 
  while (clone.status == "creating") do
    sleep 1
    clone = vs.get_volume(clone.id)
  #  print "."
  #  STDOUT.flush
  end
  puts [Time.now.to_i, clone.display_name, clone.size, clone.status,( Time.now - time_start).to_i].join(", ")
  
  STDOUT.flush
end
  
puts solid.csv


