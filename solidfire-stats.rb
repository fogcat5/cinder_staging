#!/usr/bin/ruby

require 'rubygems'
require 'rest-client'


# 7:53:12 - { "method":"GetClusterCapacity", "params":{ }, "id":21 }
# 7:53:10 - { "method":"GetClusterStats", "params":{ }, "id":20 }
# 
# 
# 
# 7:53:43 - { "id":30, "result":{ "clusterCapacity":{ "activeBlockSpace":163277424416, "activeSessions":7, "averageIOPS":0, "clusterRecentIOSize":0, "currentIOPS":0, "maxIOPS":250000, "maxProvisionedSpace":172751667462144, "maxUsedSpace":27005727006720, "nonZeroBlocks":889924751, "peakActiveSessions":7, "peakIOPS":6, "provisionedSpace":7020124045312, "timestamp":"2013-07-18T06:48:55Z", "totalOps":7105666, "uniqueBlocks":59359402, "uniqueBlocksUsedSpace":140664184664, "usedSpace":163277424416, "zeroBlocks":2537870193 } } }
# 7:53:42 - { "id":29, "result":{ "clusterStats":{ "clusterUtilization":0.0, "readBytes":460776206336, "readOps":3960675, "timestamp":"2013-07-18T06:49:21.891310Z", "writeBytes":1304416063488, "writeOps":3145006 } }
# 
 
# RestClient.get 'https://user:password@example.com/private/resource', {:accept => :json}

require 'json'
require 'pp'

cmd = "GetClusterCapacity"
#cmd = "GetClusterStats"

res = RestClient.post 'https://admin:admin@10.57.160.32/json-rpc/5.0/',
 "{ \"method\":\"#{cmd}\", \"id\":\"#{Time.now.to_i}\" }"

#result = RestClient.post 'https://admin:admin@10.57.160.32/json-rpc/5.0/',
#  {"method:GetClusterCapacity"}

#RestClient.get 'http://example.com/resource', {:params => {:id => 50, 'foo' => 'bar'}}

j = JSON.parse(res)["result"]

pp j
# 
# ["activeBlockSpace",
#  "activeSessions",
#  "averageIOPS",
#  "clusterRecentIOSize",
#  "currentIOPS",
#  "maxIOPS",
#  "maxProvisionedSpace",
#  "maxUsedSpace",
#  "nonZeroBlocks",
#  "peakActiveSessions",
#  "peakIOPS",
#  "provisionedSpace",
#  "timestamp",
#  "totalOps",
#  "uniqueBlocks",
#  "uniqueBlocksUsedSpace",
#  "usedSpace",
#  "zeroBlocks"]
# 
# {"result"=>
#   {"clusterCapacity"=>
#     {"uniqueBlocks"=>59359402,
#      "activeBlockSpace"=>163277424416,
#      "zeroBlocks"=>2537870193,
#      "currentIOPS"=>0,
#      "clusterRecentIOSize"=>0,
#      "uniqueBlocksUsedSpace"=>140664184664,
#      "nonZeroBlocks"=>889924751,
#      "maxProvisionedSpace"=>172751667462144,
#      "peakIOPS"=>6,
#      "peakActiveSessions"=>7,
#      "maxUsedSpace"=>27005727006720,
#      "maxIOPS"=>250000,
#      "timestamp"=>"2013-07-18T07:31:13Z",
#      "averageIOPS"=>0,
#      "activeSessions"=>7,
#      "usedSpace"=>163277424416,
#      "totalOps"=>7107753,
#      "provisionedSpace"=>7020124045312}},
#  "id"=>"1374161747"}
# 
# 
#
# ["activeBlockSpace",
#  "nonZeroBlocks",
#  "provisionedSpace",
#  "uniqueBlocks",
#  "uniqueBlocksUsedSpace",
#  "usedSpace",
#  "zeroBlocks"]
#
