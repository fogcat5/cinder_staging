#!/usr/bin/ruby

require 'rubygems'
require 'openstack'
require 'solidfire'

require 'pp'

if ENV['OS_PASSWORD'].nil?
  puts "please source your openrc or at least set OS_PASSWORD before running this script"
  exit 1
end

Aurora_solidfire = Solidfire.new("10.57.160.32","admin","admin")


Volume_service = OpenStack::Connection.create({:username => ENV['OS_USERNAME'] || ENV['USER'], 
	:api_key => ENV['OS_PASSWORD'],
	:auth_url => "http://keystone-vip.qa.paypal.com:5000/v2.0", 
	:authtenant=>ENV['OS_TENANT_NAME'] || ENV['USER'], :service_type=>"volume"})

