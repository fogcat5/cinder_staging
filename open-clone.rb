#!/usr/bin/ruby
require 'aurora'
require 'pp'

solid = Aurora_solidfire

puts solid.heading
puts solid.csv

vs = Volume_service

# create a volume or find it by name

# time how long it takes to clone that volume

# work out how many volumes exist.
# reset or clear the volumes out
# create 100 and time each/overall time

# check the solidfire status for efficiency, usage, etc. between runs
# save the results overall
# log_solidfire('clone volume') do 
#   vol.create(volname, source volume, size)
# end
#
# def log_solidfire(tag) 
#   st = Time.now
#   yield
#   t = Time.now - st
#   fetch_solidfire_data
#   write_log(tag, t, solidfirestats)
#



# do this in ruby

#cinder create --volume-type solidfire --display_name dataclone001 --source-volid 4fdc1df1-f8b6-472b-ace7-bae3cbf23981 400

clone = vs.create_volume(:display_name => "dataclone901",
	:volume_type => "solidfire",
	:size => 400,
	:source_volid => "4fdc1df1-f8b6-472b-ace7-bae3cbf23981")


time_start = Time.now
puts "clone status #{clone.status}" 
while (clone.status == "creating") do
  sleep 1
  clone = vs.get_volume(clone.id)
  print "."
  STDOUT.flush
end
puts "\nstatus #{clone.status}: #{Time.now - time_start} seconds"
pp clone

puts solid.csv

