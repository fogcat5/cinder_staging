#!/usr/bin/ruby

require 'solidfire'
require 'pp'

class Solidfire
  def adjust_qos(id)
#d":"ModifyVolume",    "params":{        "volumeID":10407,        "qos":{            "minIOPS":100,            "maxIOPS":34000,            "burstIOPS":50000        }    },    "id":10}
    params = Hash.new
    qos = Hash.new
    qos["maxIOPS"] = 35000
    qos["burstIOPS"] = 50000
    qos["minIOPS"] = 100
    params["qos"]=qos
    params["volumeID"] = id
    post("ModifyVolume",params)
  end
end

sf = Solidfire.new("10.57.160.32","admin","admin")

sf.volumes.each do |v|
  q = OpenStruct.new(v["qos"])
  if q.maxIOPS < 20000
    puts "adjusting QOS for #{v['name']} was #{q.maxIOPS}"
    pp sf.adjust_qos(v["volumeID"])    # adjust IOPS to 35000 50000
  else
    puts "#{v['name']} #{q.maxIOPS}"
  end
end


